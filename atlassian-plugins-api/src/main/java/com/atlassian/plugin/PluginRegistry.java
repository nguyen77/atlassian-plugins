package com.atlassian.plugin;

import java.util.Collection;

public interface PluginRegistry {
    interface ReadOnly {
        Collection<Plugin> getAll();

        Plugin get(final String key);
    }

    interface ReadWrite extends ReadOnly {
        void clear();

        void put(final Plugin plugin);

        Plugin remove(final String key);
    }
}
