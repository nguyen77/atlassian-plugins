package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

/**
 * Signifies a plugin module is now available outside the usual installation process.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.5.0
 */
@PublicApi
public class PluginModuleAvailableEvent extends PluginModuleEvent {
    public PluginModuleAvailableEvent(final ModuleDescriptor module) {
        super(module);
    }
}
