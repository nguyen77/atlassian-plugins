package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event fired before a plugin is explicitly uninstalled (as opposed to as part of an upgrade).
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginUninstallingEvent extends PluginEvent {
    public PluginUninstallingEvent(final Plugin plugin) {
        super(plugin);
    }
}
