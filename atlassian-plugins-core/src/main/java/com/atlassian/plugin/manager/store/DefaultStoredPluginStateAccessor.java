package com.atlassian.plugin.manager.store;

import com.atlassian.plugin.StoredPluginStateAccessor;
import com.atlassian.plugin.manager.PluginPersistentStateStore;

import static java.util.Objects.requireNonNull;

/**
 * Default implementation of {@link StoredPluginStateAccessor} -- delegates to {@link PluginPersistentStateStore}.
 *
 * @since 5.1.0
 */
public class DefaultStoredPluginStateAccessor implements StoredPluginStateAccessor {

    private final PluginPersistentStateStore pluginPersistentStateStore;

    public DefaultStoredPluginStateAccessor(PluginPersistentStateStore pluginPersistentStateStore) {
        this.pluginPersistentStateStore = requireNonNull(pluginPersistentStateStore);
    }

    @Override
    public com.atlassian.plugin.StoredPluginState get() {
        return pluginPersistentStateStore.load();
    }
}
