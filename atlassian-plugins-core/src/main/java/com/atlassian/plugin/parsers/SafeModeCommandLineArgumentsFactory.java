package com.atlassian.plugin.parsers;

/**
 * Interface for {@link SafeModeCommandLineArguments} factory, this instantiates an instance of
 * {@link SafeModeCommandLineArguments} with the appropriate parameters
 */
public interface SafeModeCommandLineArgumentsFactory {

    /**
     * @return reference to the SafeModeCommandLineArguments singleton
     */
    SafeModeCommandLineArguments get();
}
