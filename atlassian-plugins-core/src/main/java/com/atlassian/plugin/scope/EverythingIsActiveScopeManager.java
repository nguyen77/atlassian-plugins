package com.atlassian.plugin.scope;

/**
 * Allows scope checks to be introduced ahead of TCS based implementation
 *
 * @since 4.1
 * @deprecated in 5.0 for removal in 6.0 when {@link ScopeManager} is removed completely.
 */
@Deprecated
public class EverythingIsActiveScopeManager implements ScopeManager {

    @Override
    public boolean isScopeActive(String scopeKey) {
        return true;
    }
}
