package com.atlassian.plugin.manager;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginDependencies;
import com.google.common.collect.Collections2;
import org.junit.Before;

import java.util.HashSet;
import java.util.Set;

import static com.atlassian.plugin.manager.PluginWithDeps.GET_KEY;

public class TestDependentPlugins {
    protected Set<Plugin> allPlugins;

    @Before
    public void setUp() throws Exception {
        allPlugins = new HashSet<>();

        //  X --- uses --(m - mandatory, etc.) Y

        // A ---m B ---d C --m G
        //    \_o D __/o
        //    \_d E _/m

        // F ---d B
        //    \_m D
        //    \_o E

        // H ---o B
        //    \_d D
        //    \_m E

        allPlugins.add(new PluginWithDeps("A", PluginDependencies.builder()
                .withMandatory("B")
                .withOptional("D")
                .withDynamic("E")
                .build()));

        allPlugins.add(new PluginWithDeps("F", PluginDependencies.builder()
                .withDynamic("B")
                .withMandatory("D")
                .withOptional("E")
                .build()));

        allPlugins.add(new PluginWithDeps("H", PluginDependencies.builder()
                .withOptional("B")
                .withDynamic("D")
                .withMandatory("E")
                .build()));

        allPlugins.add(new PluginWithDeps("B", PluginDependencies.builder()
                .withDynamic("C")
                .build()));

        allPlugins.add(new PluginWithDeps("D", PluginDependencies.builder()
                .withOptional("C")
                .build()));

        allPlugins.add(new PluginWithDeps("E", PluginDependencies.builder()
                .withMandatory("C")
                .build()));

        allPlugins.add(new PluginWithDeps("C", PluginDependencies.builder()
                .withMandatory("G")
                .build()));

        allPlugins.add(new PluginWithDeps("G"));

        // circular a -m b -m c -m a
        allPlugins.add(new PluginWithDeps("a", PluginDependencies.builder()
                .withMandatory("b")
                .build()));
        allPlugins.add(new PluginWithDeps("b", PluginDependencies.builder()
                .withMandatory("c")
                .build()));
        allPlugins.add(new PluginWithDeps("c", PluginDependencies.builder()
                .withMandatory("a")
                .build()));

        // circular AC -o AC
        //          AC /d
        //          AC /m

        allPlugins.add(new PluginWithDeps("aC", PluginDependencies.builder()
                .withOptional("aC")
                .withDynamic("aC")
                .withMandatory("aC")
                .build()));

        // Multiple dependency types

        // mX ---m mA ---m mB ---d mC
        //             \_o mD __/o
        //             \_d mE _/m

        // mA ---d mB
        //     \_m mD
        //     \_o mE

        // mA ---o mB
        //     \_d mD
        //     \_m mE

        allPlugins.add(new PluginWithDeps("mA", PluginDependencies.builder()
                .withMandatory("mB", "mD", "mE")
                .withOptional("mB", "mD", "mE")
                .withDynamic("mB", "mD", "mE")
                .build()));

        allPlugins.add(new PluginWithDeps("mB", PluginDependencies.builder()
                .withDynamic("mC")
                .build()));

        allPlugins.add(new PluginWithDeps("mD", PluginDependencies.builder()
                .withOptional("mC")
                .build()));

        allPlugins.add(new PluginWithDeps("mE", PluginDependencies.builder()
                .withMandatory("mC")
                .build()));

        allPlugins.add(new PluginWithDeps("mX", PluginDependencies.builder()
                .withMandatory("mA")
                .build()));

        allPlugins.add(new PluginWithDeps("mC", PluginDependencies.builder()
                .withMandatory("mE")
                .withOptional("mD")
                .withDynamic("mB")
                .build()));
    }

    protected Set<String> getKeys(final Set<Plugin> plugins) {
        return new HashSet<>(Collections2.transform(plugins, GET_KEY));
    }

}
