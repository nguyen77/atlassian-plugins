package com.atlassian.plugin.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestContextClassLoaderSwitchingUtil {
    @Mock
    private ClassLoader newLoader;

    @Test
    public void testSwitchClassLoader() {
        ClassLoader currentLoader = Thread.currentThread().getContextClassLoader();
        ContextClassLoaderSwitchingUtil.runInContext(newLoader, () -> {
            assertEquals(newLoader, Thread.currentThread().getContextClassLoader());
            newLoader.getResource("test");
        });

        // Verify the loader is set back.
        assertEquals(currentLoader, Thread.currentThread().getContextClassLoader());

        // Verify the code was actually called
        verify(newLoader).getResource("test");
    }

    @Test
    public void testSwitchClassLoaderMultiple() {
        ClassLoader currentLoader = Thread.currentThread().getContextClassLoader();
        ContextClassLoaderSwitchingUtil.runInContext(newLoader, () -> {
            assertEquals(newLoader, Thread.currentThread().getContextClassLoader());
            newLoader.getResource("test");
            final ClassLoader innerLoader = mock(ClassLoader.class);
            ClassLoader currentLoader1 = Thread.currentThread().getContextClassLoader();
            ContextClassLoaderSwitchingUtil.runInContext(innerLoader, () -> {
                assertEquals(innerLoader, Thread.currentThread().getContextClassLoader());
                innerLoader.getResource("test");
            });

            // Verify the loader is set back.
            assertEquals(currentLoader1, Thread.currentThread().getContextClassLoader());

            // Verify the code was actually called
            verify(newLoader).getResource("test");
        });

        // Verify the loader is set back.
        assertEquals(currentLoader, Thread.currentThread().getContextClassLoader());

        // Verify the code was actually called
        verify(newLoader).getResource("test");
    }
}
