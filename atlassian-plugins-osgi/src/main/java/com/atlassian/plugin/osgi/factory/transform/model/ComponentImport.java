package com.atlassian.plugin.osgi.factory.transform.model;

import com.atlassian.plugin.PluginParseException;
import org.dom4j.Element;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.plugin.util.validation.ValidationPattern.createPattern;
import static com.atlassian.plugin.util.validation.ValidationPattern.test;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents the data in a component-import tag in the plugin descriptor
 *
 * @since 2.2.0
 */
public class ComponentImport {
    private final String key;
    private final Set<String> interfaces;
    private final String filter;
    private final Element source;

    public ComponentImport(Element element) throws PluginParseException {
        checkNotNull(element);
        createPattern().
                rule(
                        test("@key").withError("The key is required"),
                        test("(@interface and string-length(@interface) > 0) or (interface and string-length(interface[1]) > 0)")
                                .withError("The interface must be specified either via the 'interface'" +
                                        "attribute or child 'interface' elements")).
                evaluate(element);

        this.source = element;
        this.key = element.attributeValue("key").trim();
        final String filter = element.attributeValue("filter");
        this.filter = filter != null ? filter.trim() : null;
        this.interfaces = new LinkedHashSet<>();
        if (element.attribute("interface") != null) {
            interfaces.add(element.attributeValue("interface").trim());
        } else {
            List<Element> compInterfaces = element.elements("interface");
            for (Element inf : compInterfaces) {
                interfaces.add(inf.getTextTrim());
            }
        }
    }

    public String getKey() {
        return key;
    }

    public Set<String> getInterfaces() {
        return interfaces;
    }

    public Element getSource() {
        return source;
    }

    /**
     * @return The configured ldap filter
     * @since 2.3.0
     */
    public String getFilter() {
        return filter;
    }
}
