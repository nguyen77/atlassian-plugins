package com.atlassian.plugin.osgi;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.google.common.annotations.VisibleForTesting;

public class DummyModuleDescriptor extends AbstractModuleDescriptor<Void> {
    private boolean enabled = false;

    // required to silence log errors when using DefaultHostContainer in tests
    @VisibleForTesting
    public DummyModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    public DummyModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public Void getModule() {
        return null;
    }

    @Override
    public void enabled() {
        super.enabled();
        enabled = true;

    }

    public boolean isEnabled() {
        return enabled;
    }

}
